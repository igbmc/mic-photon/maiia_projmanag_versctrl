# Files and Folders control

## Digital ecosystem to organize and keep track of your projects

From master students to post-docs, from researchers to staff members, from secretaries to entrepreneurs; almost everybody nowadays has to deal with **project management**. In this century, especially in research, this most often means **produce, organize and keep track** of digital files and folders. However, not only is project management one of the most _underemphasized_ skills to learn; the organization and documentation of files and folders is mostly forgotten and/or postponed until it's too late. This too often leads to loss of information, time and precious scientific data. Simply ask yourself, how easily could you find and reproduce a project you worked on a couple of years ago?

Admittedly, this process is boring, painful and often difficult. On the other hand, modern tools and strategies have provided solutions and support for this problem, although they are still largely unknown to many researchers (especially in the biology field). This talk tries to present an overview of some solutions and tools aimed to simplify, and accelerate file, folders and projects management especially in the context of scientific research and data analysis. Some examples include: Jupyter notebooks, Quarto publishing framework, Git versioning and Markdown/LaTeX. We will see how these tools can work together inside a project to guarantee excellent structure, understandability and reproducibility of any digital project.

## What is this?

This is a presentation / talk and repository example with some real-world cases of typical challenges regarding digital project, files and folders management. I tried to showcase several tools and strategies on how to improve the organization and control of a digital project. These include:

1. [LaTeX](https://www.latex-project.org/)
2. [Markdown](https://www.markdownguide.org/)
3. [Jupyter notebooks](https://jupyter.org/)
4. [Quarto](https://quarto.org/)
5. [Git](https://git-scm.com/) / version control

## Content

### projmanag_examples

Contains several examples of folder architecture for a digital project, in the presentation I discuss the advantages and disadvantages of this organizations. These folders are paired with a visual graph-like representations of their structures I obtained thanks to the dir.py script from this Github repository.

### big_project

This contains an example large project folder with real-case computational and experimental analysis, their documentation and possible presentations and publication strategies, which showcase and combine the use of several tools, described in the presentation.

### presentation

This contains the source material and the HTML final presentation presented in this talk. To see the presentation directly click [here](https://igbmc.gitlab.io/mic-photon/maiia_projmanag_versctrl).

## Author

Marco Dalla Vecchia dallavem@igbmc.fr
